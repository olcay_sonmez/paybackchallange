package com.android.paybackchallange.model

data class ImageHitsResponse(val hits: ArrayList<SearchImageResponse>?)