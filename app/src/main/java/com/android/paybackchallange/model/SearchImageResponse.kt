package com.android.paybackchallange.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchImageResponse(
    @SerializedName("previewURL") val imageUrl: String?,
    @SerializedName("largeImageURL") val largeImageUrl: String?,
    @SerializedName("user") val userName: String?,
    val tags: String?,
    val comments: Int?,
    val likes: Int?,
    val favorites: Int?
) : Parcelable

