package com.android.paybackchallange.di

import com.android.paybackchallange.ui.list.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class])
interface PayBackComponent {
    fun inject(activity: MainActivity)
}