package com.android.paybackchallange.network

import com.android.paybackchallange.model.ImageHitsResponse
import com.android.paybackchallange.model.SearchImageResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ImageApi {

    @GET("/api/")
    fun searchImage(@QueryMap queryMap: Map<String, String>): Single<ImageHitsResponse>
}