package com.android.paybackchallange.network

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class ImageGlideLoader: AppGlideModule()