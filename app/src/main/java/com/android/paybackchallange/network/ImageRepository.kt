package com.android.paybackchallange.network

import android.util.Xml
import com.android.paybackchallange.model.Constant
import com.android.paybackchallange.model.ImageHitsResponse
import io.reactivex.Single
import java.net.URLEncoder

class ImageRepository(val imageApi: ImageApi) {

    private var response: ImageHitsResponse? = null

    fun getImage(input: String): Single<ImageHitsResponse> {

        if (response != null) {
            return Single.just(response)
        } else {
            val map = mapOf(
                "key" to Constant.PIXABAY_KEY,
                "q" to URLEncoder.encode(input, Xml.Encoding.UTF_8.name),
                "image_type" to "photo"
            )


            return imageApi.searchImage(map).doOnSuccess { response ->
                this.response = response
            }
        }
    }

    fun removeResponse() {
        response = null
    }
}