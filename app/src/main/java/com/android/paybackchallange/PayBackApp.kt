package com.android.paybackchallange

import android.app.Application
import com.android.paybackchallange.di.DaggerPayBackComponent
import com.android.paybackchallange.di.NetworkModule
import com.android.paybackchallange.di.PayBackComponent

class PayBackApp : Application() {

    private var payBackComponent: PayBackComponent? = null

    override fun onCreate() {
        super.onCreate()
        payBackComponent = DaggerPayBackComponent
            .builder()
            .networkModule(NetworkModule())
            .build()
    }

    fun getPayBackComponent() = payBackComponent
}