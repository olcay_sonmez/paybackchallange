package com.android.paybackchallange.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.paybackchallange.R
import com.android.paybackchallange.model.SearchImageResponse

class ImageListAdapter(
    var list: ArrayList<SearchImageResponse>?,
    var clickListener: ItemClickListener?
) :
    RecyclerView.Adapter<ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.image_list_layout, parent, false)
        return ImageViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        list?.let { holder.bindData(it[position]) }
    }

    fun setData(list: ArrayList<SearchImageResponse>) {
        this.list?.clear()
        this.list = list
        notifyDataSetChanged()
    }
}