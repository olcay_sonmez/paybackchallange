package com.android.paybackchallange.ui.list

import android.util.Log
import com.android.paybackchallange.base.BasePresenter
import com.android.paybackchallange.network.ImageRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainPresenter(val repository: ImageRepository) : BasePresenter<MainContract.View>(),
    MainContract.Presenter {

    override fun searchImage(input: String, isNewSearch: Boolean) {

        view?.showProgressBar(true)
        view?.showRecyclerView(false)
        view?.hideInfoText(true)

        if (isNewSearch) {
            repository.removeResponse()
        }

        getCompositeDisposable().add(
            repository.getImage(input)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { response ->
                        view?.showProgressBar(false)
                        response?.hits?.takeIf { it.isNotEmpty() }?.let {
                            view?.showRecyclerView(true)
                            view?.updateData(it)
                        } ?: run {
                            view?.showRecyclerView(false)
                            view?.hideInfoText(false)
                            view?.setInfoText("No Result")
                            view?.updateData(arrayListOf())
                        }
                    }, { t ->
                        view?.showRecyclerView(false)
                        view?.showProgressBar(false)
                        view?.hideInfoText(false)
                        view?.setInfoText("an error has occurred")
                        Log.e("MainPresenter", "throwable on api ${t.message}")
                    })
        )
    }
}