package com.android.paybackchallange.ui.detail


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment

import com.android.paybackchallange.R
import com.android.paybackchallange.model.Constant
import com.android.paybackchallange.model.SearchImageResponse
import com.android.paybackchallange.network.GlideApp
import kotlinx.android.synthetic.main.fragment_detail_dialog.*


class DetailDialogFragment : DialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(item: SearchImageResponse): DetailDialogFragment {
            val fragment = DetailDialogFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(Constant.DETAIL_ITEM, item)
            }
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { dismiss() }
        arguments?.getParcelable<SearchImageResponse>(Constant.DETAIL_ITEM)?.let {
            setUpView(it)
        } ?: dismiss()
    }

    private fun setUpView(item: SearchImageResponse) {
        context?.let {
            GlideApp.with(it).load(item.largeImageUrl).fitCenter().into(result_image_view)
        }
        user_name_text_view.text = item.userName
        image_tag_text_view.text = item.tags

        like_count_text_view.text = getString(R.string.like_count, item.likes ?: 0)
        favourite_count_text_view.text = getString(R.string.favourites_count, item.favorites ?: 0)
        comment_count_text_view.text = getString(R.string.comments_count, item.comments ?: 0)
    }

    override fun getTheme(): Int {
        return R.style.NoActionBar_FullScreenDialog
    }
}
