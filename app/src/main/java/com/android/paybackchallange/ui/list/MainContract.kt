package com.android.paybackchallange.ui.list

import com.android.paybackchallange.base.BaseView
import com.android.paybackchallange.model.SearchImageResponse

interface MainContract {

    interface View : BaseView {
        fun updateData(list: ArrayList<SearchImageResponse>)
        fun showProgressBar(shouldShown: Boolean)
        fun showRecyclerView(shouldShown: Boolean)
        fun hideInfoText(shouldHide: Boolean)
        fun setInfoText(info: String)
    }

    interface Presenter {
        fun searchImage(input: String, isNewSearch: Boolean)
    }
}