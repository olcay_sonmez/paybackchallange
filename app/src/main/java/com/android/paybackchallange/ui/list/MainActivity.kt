package com.android.paybackchallange.ui.list

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.SearchView
import com.android.paybackchallange.PayBackApp
import com.android.paybackchallange.R
import com.android.paybackchallange.model.Constant
import com.android.paybackchallange.model.SearchImageResponse
import com.android.paybackchallange.network.ImageRepository
import com.android.paybackchallange.ui.detail.DetailDialogFragment
import com.android.paybackchallange.ui.list.adapter.ImageListAdapter
import com.android.paybackchallange.ui.list.adapter.ItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View, ItemClickListener {

    @Inject
    lateinit var repository: ImageRepository

    lateinit var presenter: MainPresenter

    private var adapter: ImageListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as? PayBackApp)?.getPayBackComponent()?.inject(this)
        presenter = MainPresenter(repository)
        presenter.attach(this)
        setUpView()
    }

    private fun setUpView() {
        presenter.searchImage(Constant.DEFAULT_INPUT,false)
        adapter = ImageListAdapter(arrayListOf(), this)
        image_result_recycler_view.adapter = adapter
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (Intent.ACTION_SEARCH == intent?.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { input ->
                presenter.searchImage(input, true)
            }
        }
    }

    override fun showRecyclerView(shouldShown: Boolean) {
        image_result_recycler_view.visibility = if (shouldShown) View.VISIBLE else View.GONE
    }

    override fun updateData(list: ArrayList<SearchImageResponse>) {
        adapter?.setData(list)
    }

    override fun showProgressBar(shouldShown: Boolean) {
        progress_bar.visibility = if (shouldShown) View.VISIBLE else View.GONE
    }

    override fun setInfoText(info: String) {
        message_text_view.text = info
    }

    override fun hideInfoText(shouldHide: Boolean) {
        message_text_view.visibility = if (shouldHide) View.GONE else View.VISIBLE
    }

    override fun onItemClick(result: SearchImageResponse) {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialog_title)
            .setMessage(R.string.dialog_message)
            .setPositiveButton(R.string.dialog_positive_button_name) { _, _ ->
                DetailDialogFragment.newInstance(result).show(
                    supportFragmentManager,
                    "DetailDialogFragment"
                )
            }
            .create()
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu?.findItem(R.id.search)?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(false)
        }
        return super.onCreateOptionsMenu(menu)
    }
}
