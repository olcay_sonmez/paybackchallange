package com.android.paybackchallange.ui.list.adapter

import com.android.paybackchallange.model.SearchImageResponse

interface ItemClickListener {
    fun onItemClick(result: SearchImageResponse)
}