package com.android.paybackchallange.ui.list.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.paybackchallange.model.SearchImageResponse
import com.android.paybackchallange.network.GlideApp
import kotlinx.android.synthetic.main.image_list_layout.view.*

class ImageViewHolder(itemView: View, private val clickListener: ItemClickListener?) :
    RecyclerView.ViewHolder(itemView) {

    fun bindData(result: SearchImageResponse) {
        GlideApp.with(itemView.context).load(result.imageUrl)
            .into(itemView.image_thumbnail_image_view)

        itemView.user_name_text_view.text = result.userName
        itemView.tag_text_view.text = result.tags

        itemView.setOnClickListener {
            clickListener?.onItemClick(result)
        }
    }
}