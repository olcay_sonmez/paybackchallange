package com.android.paybackchallange.base

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V : BaseView> {

    var view: V? = null
    private val compositeDisposable = CompositeDisposable()

    val isViewAttached: Boolean
        get() = view != null

    fun attach(view: V) {
        this.view = view
    }

    fun detach() {
        view = null
        compositeDisposable.clear()
    }

    fun getCompositeDisposable() = compositeDisposable
}